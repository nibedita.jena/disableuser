var express = require('express');
var router = express.Router();
var mysql  = require('promise-mysql');
const moment = require('moment');
const tz = require('moment-timezone');
const { check, validationResult } = require('express-validator');
const {BigQuery} = require('@google-cloud/bigquery');
const bigquery = new BigQuery({projectId: 'iserveuprod',keyFilename: 'iServeUProd-1f8372f23af4.json'});//iserveuprod.production.user_details

//MYSQL STAGING DATABASE CREDENTIAL: {user: 'root', password: 'I@serve#U', database: 'isu_db4', host: '35.200.192.22', port: 3306} 
var user_name=check('user_name').not().isEmpty().withMessage('user name should not be blank').isString().withMessage("user name must be a string")
  
router.post('/', user_name,function(req, res, next) {
    var user_name = req.body.user_name;
    const errors = validationResult(req)
  if (!errors.isEmpty()) {
    return res.status(422).json({ "errors": errors.array(),"status":-1 })
  }
async function mysqldataTobigquery(){
    const pool = await mysql.createPool({
        user:'root',
        password:'I@serve#U',
        database:'isu_db4',
        host: '35.200.192.22', 
        port: 3306
    });
     const data=['0',user_name]  
     const updateresult = await pool.query('UPDATE user SET enabled =? WHERE user_name=?', data,function(error, rows, fields) {
      
          if(!(rows.affectedRows>0)){
            res.status(200).json({"message":"Oops!! There is no such type of username exists","status":-1})
            return;
          }
          console.log('Rows affected:', rows.affectedRows);
          res.status(200).json({"message":"Rows affected are:" +rows.affectedRows,"status":0})
          return;
        });
        //console.log(updateresult);
    //     const selectresult = await pool.query('SELECT user_name,enabled FROM user WHERE user_id=77', function(error, rows, fields) {
    //         if (error){
    //             return console.error(error.message);
    //           }
    //           console.log('Results:', rows);
    //           res.status(200).json({"message":rows,"status":200})
    //       return;
    //         });
    // console.log(selectresult);
    pool.end();

}
mysqldataTobigquery()
async function insertmysqldataToBigquery(){
    //insert same data into bigquery table
// Inserts the JSON objects into my_dataset:my_table.
const datasetId = 'production';
const tableId = 'user_details';
var created_date = moment().tz('Asia/Kolkata').format("YYYY-MM-DD 18:30:00");
console.log(created_date);
const insertrows = [
 //username, enabled, create time
 {user_name:user_name,enabled:false, created_date:created_date}
];

console.log(insertrows);
// Insert data into a table
await bigquery
  .dataset(datasetId)
  .table(tableId)
  .insert(insertrows);
console.log(`Inserted ${insertrows.length} rows`);
}
insertmysqldataToBigquery()
// .then(insertRes=>{
    
//     console.log(insertRes);
//     res.status(200).json({"message":"Inserted rows","status":200});
//     return;
//    }).catch(error=>{
//     res.status(400).json({"errors":error,"status":400});
//     return;
//    });
});

module.exports = router;
